# Datahub Kafka Consumer

Datahub에서 Metadata Ingestion시 Kafka로 전송되는 메세지를 분석하기 위한 테스트

## 참고 문서

### Spring for Apache Kafka

https://docs.spring.io/spring-kafka/reference/html/

### Receiving Messages

You can receive messages by configuring a MessageListenerContainer and providing a message listener or by using the @KafkaListener annotation.

https://docs.spring.io/spring-kafka/reference/html/#receiving-messages

### Avro Schema Serializer and Deserializer (Schema Registry)

https://docs.confluent.io/platform/current/schema-registry/serdes-develop/serdes-avro.html#avro-schema-serializer-and-deserializer

### SPECIFIC_AVRO_READER_CONFIG

By default, each record is deserialized into an Avro GenericRecord, but in this tutorial the record should be deserialized using the application’s code-generated Payment class. Therefore, configure the deserializer to use Avro SpecificRecord, i.e., SPECIFIC_AVRO_READER_CONFIG should be set to true.

https://docs.confluent.io/platform/current/schema-registry/schema_registry_onprem_tutorial.html#java-consumers



