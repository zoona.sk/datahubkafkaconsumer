package com.skcc.aidata.datahubkafkaconsumer;

import com.linkedin.pegasus2avro.mxe.MetadataChangeLog;
import com.linkedin.pegasus2avro.mxe.MetadataChangeProposal;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import lombok.extern.java.Log;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;

import java.util.HashMap;
import java.util.Map;

// References
// 1. Consumer
// https://docs.spring.io/spring-kafka/reference/html/#receiving-messages
// You can receive messages by configuring a MessageListenerContainer
// and providing a message listener or by using the @KafkaListener annotation.
//
// 2. Schema Registry
// https://docs.confluent.io/platform/current/schema-registry/serdes-develop/serdes-avro.html#avro-schema-serializer-and-deserializer
// https://docs.confluent.io/platform/current/schema-registry/schema_registry_onprem_tutorial.html#java-consumers
// KafkaAvroSerializer
// SPECIFIC_AVRO_READER_CONFIG = true


@EnableKafka
@Configuration
@Log
public class KafkaConsumerConfig {

    @Value(value = "${kafka.bootstrapAddress}")
    private String bootstrapAddress;

    @Value(value = "${kafka.schemaRegistryUrl}")
    private String schemaRegistryUrl;

    @Bean
    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, MetadataChangeProposal>>
    kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, MetadataChangeProposal> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        // consumer thread num
        factory.setConcurrency(1);
        factory.getContainerProperties().setPollTimeout(3000);
        return factory;
    }

    @Bean
    public ConsumerFactory<String, MetadataChangeProposal> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        // Schema Registry
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "io.confluent.kafka.serializers.KafkaAvroDeserializer");
        props.put("schema.registry.url", schemaRegistryUrl);
        // SPECIFIC_AVRO_READER_CONFIG를 true로 해줘야 AVRO Generated Class로 Deserialize 됨
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, true);
        return new DefaultKafkaConsumerFactory<>(props);
    }

    // MetadataChangeProposal_v1
    @KafkaListener(
            id = "MCP",
            topics = "${kafka.mcpTopicName}",
            concurrency = "1")
    public void consumeMCP(ConsumerRecord<String, MetadataChangeProposal> consumerRecord) {

        MetadataChangeProposal metadataChangeProposal = consumerRecord.value();
        PrintEventUtil.printMCPWithoutAspectShorty(metadataChangeProposal);
        PrintEventUtil.printMCPAspect(metadataChangeProposal);
    }

    // MetadataChangeLog_Timeseries_v1
    // MetadataChangeLog_Timeseries_v1, MetadataChangeLog_Versioned_v1 스키마가
    // MetadataChangeLog 한개로 동일
    @KafkaListener(
            id = "MCLT",
            topics = "${kafka.mcltTopicName}",
            concurrency = "1")
    public void consumeMCLT(ConsumerRecord<String, MetadataChangeLog> consumerRecord) {
        MetadataChangeLog metadataChangeLog = consumerRecord.value();
        PrintEventUtil.printMCLWithoutAspectShorty("MCLT", metadataChangeLog);
        PrintEventUtil.printMCIAspect(metadataChangeLog);
    }

    // MetadataChangeLog_Versioned_v1
    @KafkaListener(
            id = "MCLV",
            topics = "${kafka.mclvTopicName}",
            concurrency = "1")
    public void consumeMCLV(ConsumerRecord<String, MetadataChangeLog> consumerRecord) {
        MetadataChangeLog metadataChangeLogVersioned = consumerRecord.value();
        PrintEventUtil.printMCLWithoutAspectShorty("MCLV", metadataChangeLogVersioned);
        PrintEventUtil.printMCIAspect(metadataChangeLogVersioned);
    }

}