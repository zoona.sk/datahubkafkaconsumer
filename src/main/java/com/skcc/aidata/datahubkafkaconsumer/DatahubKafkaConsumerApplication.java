package com.skcc.aidata.datahubkafkaconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatahubKafkaConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatahubKafkaConsumerApplication.class, args);
	}

}
