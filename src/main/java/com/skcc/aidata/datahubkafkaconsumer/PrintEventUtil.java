package com.skcc.aidata.datahubkafkaconsumer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.linkedin.pegasus2avro.mxe.MetadataChangeLog;
import com.linkedin.pegasus2avro.mxe.MetadataChangeProposal;
import lombok.extern.java.Log;

import java.nio.charset.StandardCharsets;

@Log
public class PrintEventUtil {

    private PrintEventUtil() {
        throw new IllegalStateException("Utility class");
    }
    // 출력용 함수들
    // 필드 중 Aspect만 JSON String 형태기 때문에 가독성이 떨어짐
    // Aspect를 제외한 필드 출력, Aspect를 Json Pretty Printing 하는 함수로 나눠서 사용
    public static void printMCLWithoutAspect(String eventType, MetadataChangeLog v) {
        log.info(String.format(
                "[%s] auditHeader: %s, entityType: %s, entityUrl: %s, entityKeyAspect: %s, " +
                        "changeType: %s, aspectName: %s, systemMetadata: %s", eventType,
                v.getAuditHeader(), v.getEntityType(), v.getEntityUrn(), v.getEntityKeyAspect(),
                v.getChangeType(), v.getAspectName(), v.getSystemMetadata()));
    }

    public static void printMCPWithoutAspect(MetadataChangeProposal v) {
        log.info(String.format(
                "[MCP] auditHeader: %s, entityType: %s, entityUrl: %s, entityKeyAspect: %s, " +
                        "changeType: %s, aspectName: %s, systemMetadata: %s",
                v.getAuditHeader(), v.getEntityType(), v.getEntityUrn(), v.getEntityKeyAspect(),
                v.getChangeType(), v.getAspectName(), v.getSystemMetadata()));
    }

    // 분석 상 큰 의미 없는 필드는 제외하고 출력
    public static void printMCLWithoutAspectShorty(String eventType, MetadataChangeLog v) {
        log.info(String.format(
                "[%s] entityType: %s, aspectName: %s, entityUrl: %s", eventType,
                v.getEntityType(), v.getAspectName(), v.getEntityUrn()));
    }

    public static void printMCPWithoutAspectShorty(MetadataChangeProposal v) {
        log.info(String.format(
                "[MCP] entityType: %s, aspectName: %s, entityUrl: %s",
                v.getEntityType(), v.getAspectName(), v.getEntityUrn()));
    }
    public static void printMCPAspect(MetadataChangeProposal v) {
        if(v.getAspect() == null) {
            log.info("null Aspect in MetadataChangeLog");
            return;
        }

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String s = StandardCharsets.UTF_8.decode(v.getAspect().getValue()).toString();
        JsonElement jsonElement = JsonParser.parseString(s);
        String prettyJson = gson.toJson(jsonElement);
        log.info(prettyJson);
    }

    public static void printMCIAspect(MetadataChangeLog v) {
        if(v.getAspect() == null) {
            log.info("null Aspect in MetadataChangeLog");
            return;
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String s = StandardCharsets.UTF_8.decode(v.getAspect().getValue()).toString();
        JsonElement jsonElement = JsonParser.parseString(s);
        String prettyJson = gson.toJson(jsonElement);
        log.info(prettyJson);
    }
}
